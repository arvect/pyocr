import numpy as np
import cv2
import sys, traceback
from matplotlib import pyplot as plt

try:
    img = cv2.imread('/home/oefish/Pictures/temp/digits.png')
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    img0 = cv2.imread('/tmp/ibBimage0x.png')
    img1 = cv2.imread('/tmp/ibBimage1x.png')
    img2 = cv2.imread('/tmp/ibBimage2x.png')
    img3 = cv2.imread('/tmp/ibBimage3x.png')
    img4 = cv2.imread('/tmp/ibBimage4x.png')

    gray0 = cv2.cvtColor(img0, cv2.COLOR_BGR2GRAY)
    gray1 = cv2.cvtColor(img1, cv2.COLOR_BGR2GRAY)
    gray2 = cv2.cvtColor(img2, cv2.COLOR_BGR2GRAY)
    gray3 = cv2.cvtColor(img3, cv2.COLOR_BGR2GRAY)
    gray4 = cv2.cvtColor(img4, cv2.COLOR_BGR2GRAY)

    # Now we split the image to 5000 cells, each 20x20 size
    cells = [np.hsplit(row, 100) for row in np.vsplit(gray, 50)]

    # Make it into a Numpy array. It size will be (50,100,20,20)
    x = np.array(cells)
    xx = np.array(gray0)
    np.append(xx, gray1)
    np.append(xx, gray2)
    np.append(xx, gray3)
    np.append(xx, gray4)

    # Now we prepare train_data and test_data.
    train = x[:, :50].reshape(-1, 400).astype(np.float32)  # Size = (2500,400)
    test = x[:, 50:100].reshape(-1, 400).astype(np.float32)  # Size = (2500,400)
    testxx = xx[:, :5].reshape(-1, 400).astype(np.float32)

    # Create labels for train and test data
    k = np.arange(10)
    train_labels = np.repeat(k, 250)[:, np.newaxis]
    test_labels = train_labels.copy()

    # Initiate kNN, train the data, then test it with test data for k=1
    # knn = cv2.KNearest()
    knn = cv2.ml.KNearest_create()
    knn.train(train, cv2.ml.ROW_SAMPLE, train_labels)

    ret, result, neighbours, dist = knn.findNearest(test, k=5)
    retxx, resultxx, neighboursxx, disxx = knn.findNearest(testxx, k=5)
    np.set_printoptions(threshold=np.nan)
    print(resultxx)

    # Now we check the accuracy of classification
    # For that, compare the result with test_labels and check which are wrong
    matches = result == test_labels
    correct = np.count_nonzero(matches)
    accuracy = correct * 100.0 / result.size
    print(accuracy)
except:
    traceback.print_exc(file=sys.stdout)
    # var = sys..format_exec()
    # print(var)
