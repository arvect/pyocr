#!/usr/bin/env python3
# import repackage
import cv2
import matplotlib.pyplot as plt
import numpy as np
import sys
from os import listdir, remove
from os.path import isfile, join

SZ = 20
CLASS_N = 10

# local modules
# repackage.up()
# from pathlib import Path  # if you haven't already done so
from .common import clock, mosaic
# exec(open('common.py').read())

def split2d(img, cell_size, flatten=True):
    h, w = img.shape[:2]
    sx, sy = cell_size
    cells = [np.hsplit(row, w//sx) for row in np.vsplit(img, h//sy)]
    print('--------cells.shape(), type just after vsplit and hsplit: ' + str(len(cells)) + '/' + str(len(cells[0])) + '/' + str(len(cells[0][0][0])))
    cells = np.array(cells)
    if flatten:
        cells = cells.reshape(-1, sy, sx)
    print('--------h/w: ' + str(h) + '/' + str(w))
    print('--------sx/sy/cells.size/cells.shape(): ' + str(sx) + '/' + str(sy) + '/' + str(cells.size) + '/' + format(cells.shape))
    return cells

def load_digits(fn):
    digits_img = cv2.imread(fn, 0)
    digits = split2d(digits_img, (SZ, SZ))
    labels = np.repeat(np.arange(CLASS_N), len(digits)/CLASS_N)
    return digits, labels

def deskew(img):
    m = cv2.moments(img)
    if abs(m['mu02']) < 1e-2:
        return img.copy()
    skew = m['mu11']/m['mu02']
    M = np.float32([[1, skew, -0.5*SZ*skew], [0, 1, 0]])
    img = cv2.warpAffine(img, M, (SZ, SZ), flags=cv2.WARP_INVERSE_MAP | cv2.INTER_LINEAR)
    return img

class StatModel(object):
    def load(self, fn):
        self.model.load(fn)  # Known bug: https://github.com/opencv/opencv/issues/4969
    def save(self, fn):
        self.model.save(fn)

class SVM(StatModel):
    def __init__(self, C = 12.5, gamma = 0.50625):
        self.model = cv2.ml.SVM_create()
        self.model.setGamma(gamma)
        self.model.setC(C)
        self.model.setKernel(cv2.ml.SVM_RBF)
        self.model.setType(cv2.ml.SVM_C_SVC)

    def train(self, samples, responses):
        self.model.train(samples, cv2.ml.ROW_SAMPLE, responses)

    def predict(self, samples):
        print('samples.shape/samples.size/itemsize/ndim/samples[0].size/32F: ' + format(samples.shape) + '/' + str(samples.size) + '/' + str(samples.itemsize) + '/' + str(samples.ndim) + '/' + str(samples[0].size)+ '/' + str(cv2.CV_32F))
        return self.model.predict(samples)[1].ravel()

def evaluate_model(model, digits, samples, labels):
    resp = model.predict(samples)
    err = (labels != resp).mean()
    print('Accuracy: %.2f %%' % ((1 - err)*100))

    confusion = np.zeros((10, 10), np.int32)
    for i, j in zip(labels, resp):
        confusion[int(i), int(j)] += 1
    print('confusion matrix:')
    print(confusion)

    vis = []
    for img, flag in zip(digits, resp == labels):
        img = cv2.cvtColor(img, cv2.COLOR_GRAY2BGR)
        if not flag:
            img[...,:2] = 0
        
        vis.append(img)
    return mosaic(25, vis)

def preprocess_simple(digits):
    return np.float32(digits).reshape(-1, SZ*SZ) / 255.0


def get_hog() : 
    winSize = (20,20)
    blockSize = (10,10)
    blockStride = (5,5)
    cellSize = (10,10)
    nbins = 9
    derivAperture = 1
    winSigma = -1.
    histogramNormType = 0
    L2HysThreshold = 0.2
    gammaCorrection = 1
    nlevels = 64
    signedGradient = True

    hog = cv2.HOGDescriptor(winSize,blockSize,blockStride,cellSize,nbins,derivAperture,winSigma,histogramNormType,L2HysThreshold,gammaCorrection,nlevels, signedGradient)

    return hog
    affine_flags = cv2.WARP_INVERSE_MAP|cv2.INTER_LINEAR



if __name__ == '__main__':
    strainingdirectory = '/home/oefish/029ib/pyocr/datatraining'
    sworkingdirectory  = '/home/oefish/029ib/pyocr/dataworking'

    print('Loading digits from digits.png ... ')
    # Load data.
    digits, labels = load_digits('digits.png')
    print('type, digits.shape/ type, labels.shape: ' + format(type(digits)) + '/' + format(digits.shape) + '/' + format(type(labels)) + format(labels.shape))

    print('Shuffle data ... ')
    # Shuffle data
    rand = np.random.RandomState(10)
    shuffle = rand.permutation(len(digits))
    digitspre, labelspre = digits[shuffle], labels[shuffle]
    digits, labels = digitspre[:0], labelspre[:0]
    # digits, labels = np.ndarray((0, 20, 20)), np.ndarray((0,))
    print('second round type, digits.shape/ type, labels.shape: ' + format(type(digits)) + '/' + format(digits.shape) + '/' + format(type(labels)) + format(labels.shape))

    onlyfiles = [f for f in listdir(strainingdirectory) if ( isfile(join(strainingdirectory, f)) and f.endswith('.png'))]

    for f in onlyfiles:
        try:
            labels = np.append(labels, int(f[-5:-4]))
            digits = np.append(digits, [cv2.imread(join(strainingdirectory, f), 0)], axis=0)
        except:
            print('Skip one line for file: ' + join(strainingdirectory, f))

    print('Deskew images ... ')
    digits_deskewed = list(map(deskew, digits))
    
    print('Defining HoG parameters ...')
    # HoG feature descriptor
    hog = get_hog();

    print('Calculating HoG descriptor for every image ... ')
    hog_descriptors = []
    for img in digits_deskewed:
        hog_descriptors.append(hog.compute(img))
    hog_descriptors = np.squeeze(hog_descriptors)

    # print('Spliting data into training (90%) and test set (10%)... ')
    # train_n=int(0.9*len(hog_descriptors))
    # digits_train, digits_test = np.split(digits_deskewed, [train_n])
    # hog_descriptors_train, hog_descriptors_test = np.split(hog_descriptors, [train_n])
    # labels_train, labels_test = np.split(labels, [train_n])

    hog_descriptors_train = hog_descriptors
    labels_train = labels

    # labels_train0 = np.array([6])
    # train9 = cv2.imread('/tmp/ibBimage0xxx.png', 0)
    # train0 = np.array(train9)
    # traindigits0 = np.array(train0).reshape(-1, 20, 20)
    # traindeskewed = list(map(deskew, traindigits0))
    # hog_train = []
    # for img in traindeskewed:
    #     hog_train.append(hog.compute(img))
    # hog_train = np.squeeze(hog_train)
    # hog_train = np.array(hog_train)


    print('Training SVM model ...')
    model = SVM()
    model.train(hog_descriptors_train, labels_train)

    print('Saving SVM model ...')
    model.save('digits_svm.dat')


    # print('Evaluating model ... ')
    # vis = evaluate_model(model, digits_test, hog_descriptors_test, labels_test)
    # cv2.imwrite("classification.jpg",vis)
    # plt.imshow("Vis", vis)
    # plt.show()
    # cv2.waitKey(0)

    # My ocr:
    result = {}
    resultIndex = {}
    workingFiles = [f for f in listdir(sworkingdirectory) if (isfile(join(sworkingdirectory, f)))]

    loopcnt = 0
    pre = []
    ispace = 0
    for f in workingFiles:
        if f[0:5].lower() == 'space':
            try:
                ispace = int(f[5:6])
                result[ispace] = ' '
            except:
                print('Illegal space name. Please have a postfix of number for the space position')
        elif f[-4:] == '.png':
            print("iamge file found in workingFiles: " + f)
            pre0 = cv2.imread(join(sworkingdirectory, f), 0)
            try:
                sfileindex = int(f[-5:-4])
                resultIndex[loopcnt] = sfileindex
                if loopcnt == 0 :
                    pre = np.array([pre0])
                else:
                    pre = np.append(pre, [pre0], axis=0)
                loopcnt = loopcnt + 1
            except:
                sys.stderr.write("Error png file name, serial number must be the last char beofre postfix: " + f)
                sys.exit()
    #
    # pre9 = cv2.imread('/tmp/ib1.png', 0)
    # pre8 = cv2.imread('/tmp/ib2.png', 0)
    # pre7 = cv2.imread('/tmp/ib3.png', 0)
    # pre6 = cv2.imread('/tmp/ib4.png', 0)
    # pre5 = cv2.imread('/tmp/ib5.png', 0)
    # pre4 = np.array([pre9])
    # # pre4 = np.append(pre4, [pre9], axis=0)
    # pre4 = np.append(pre4, [pre8], axis=0)
    # pre4 = np.append(pre4, [pre7], axis=0)
    # pre4 = np.append(pre4, [pre6], axis=0)
    # pre4 = np.append(pre4, [pre5], axis=0)
    # pre0 = np.array(pre4)
    pre0 = pre
    mydigits0 = np.array(pre0).reshape(-1, 20, 20)
    mydeskewed = list(map(deskew, mydigits0))
    # mydeskewed.append(list(map(deskew, mydigits1))[0])
    # mydeskewed.append(list(map(deskew, mydigits2))[0])
    # mydeskewed.append(list(map(deskew, mydigits3))[0])
    # mydeskewed.append(list(map(deskew, mydigits4))[0])
    myhogDescriptors = []
    print('mydeskewed.size / ele type: ' + str(len(mydeskewed)) + '/' + format(type(mydeskewed[0])))
    for img in mydeskewed:
        myhogDescriptors.append(hog.compute(img))
    myhogDescriptors = np.squeeze(myhogDescriptors)
    myhogDescriptors = np.array([myhogDescriptors])
    print('shape/myhogDescriptors.size/itemsize/ndim/myhogDescriptors[0].size/32F: ' + format(myhogDescriptors.shape) + '/' + str(myhogDescriptors.size) + '/' + str(
        myhogDescriptors.itemsize) + '/' + str(myhogDescriptors.ndim) + '/' + str(myhogDescriptors[0].size) + '/' + str(
        cv2.CV_32F))
    myResult = model.predict(myhogDescriptors[0])

    loopcnt = 0
    resultFile = open(join(sworkingdirectory, 'ibocrresult.txt'), 'w')
    for aResult in myResult:
        result[resultIndex[loopcnt]] = myResult[loopcnt]
        loopcnt = loopcnt + 1
    # 5 by author's method
    print(format(myResult))
    # for aResult in myResult :
    #     print('My result:   ' + aResult)
    loopcnt = 0
    while True:
        if loopcnt == len(result):
            break
        aResult = result[loopcnt]
        loopcnt = loopcnt + 1
        resultFile.write(str(aResult)[0:1] + '\n')

    # resultFile.write('while loop wirte completed.\n')
    # for aResult in result:
    #     resultFile.write(str(result[aResult])+ '\n')
